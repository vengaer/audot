# Audot

Small script for generating digraphs from code annotations, useful for visualizing automatons and the like.  

## Example

The script works with comments containing the `@dot:` annotation. It supports both C-style (i.e. `/* ... */`) and C++-style (`// ...`) comments with the caveat of C-style comments having to be both opened and closed on the same line.  

As an example, the code snippet

```C
/* @dot: state0 -> state1 [ label="valid input" ] */
/* @dot: state0 -> reject [ label="invalid input" ] */
static struct dfa_state start { ... };

/* @dot: state1 -> accept [ label="valid input" ] */
/* @dot: state1 -> reject [ label="invalid input" ] */
static struct dfa_state accept { ... };
```

generates the following digraph

![example digraph](img/example.png)
